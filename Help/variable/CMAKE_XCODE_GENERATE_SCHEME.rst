CMAKE_XCODE_GENERATE_SCHEME
---------------------------

If enabled, the Xcode generator will generate schema files. Those are
are useful to invoke analyze, archive, build-for-testing and test
actions from the command line.

.. note::

  The Xcode Schema Generator is still experimental and subject to
  change.

The following variables overwrites the default of the corresponding settings on the "Diagnostic" tab for each schema file:

- CMAKE_XCODE_ADDRESS_SANITIZER
- CMAKE_XCODE_ADDRESS_SANITIZER_STOP
- CMAKE_XCODE_THREAD_SANITIZER
- CMAKE_XCODE_THREAD_SANITIZER_STOP
- CMAKE_XCODE_UNDEFINED_BEHAVIOUR_SANITIZER
- CMAKE_XCODE_UNDEFINED_BEHAVIOUR_SANITIZER_STOP
- CMAKE_XCODE_DISABLE_MAIN_THREAD_CHECKER
- CMAKE_XCODE_MAIN_THREAD_ISSUE_STOP
- CMAKE_XCODE_MALLOC_SCRIBBLE
- CMAKE_XCODE_MALLOC_GUARD_EDGES
- CMAKE_XCODE_GUARD_MALLOC
- CMAKE_XCODE_ZOMBIE_OBJECTS
- CMAKE_XCODE_MALLOC_STACK
- CMAKE_XCODE_DYNAMIC_LINKER_API_USAGE
- CMAKE_XCODE_DYNAMIC_LIBRARY_LOADS

The following target properties will be applied on the "Info" and "Arguments" tab:

- XCODE_EXECUTABLE
- XCODE_ARGUMENTS
- XCODE_ENVIRONMENT
